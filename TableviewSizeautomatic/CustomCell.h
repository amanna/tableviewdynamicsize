//
//  CustomCell.h
//  TableviewSizeautomatic
//
//  Created by MAPPS MAC on 02/06/16.
//  Copyright © 2016 Delgence. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *myLbl;
- (CGSize)sizeOfFloatCell:(NSString*)strLbl;
@end
