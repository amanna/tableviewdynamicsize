//
//  CustomCell.m
//  TableviewSizeautomatic
//
//  Created by MAPPS MAC on 02/06/16.
//  Copyright © 2016 Delgence. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(CGSize)sizeOfFloatCell:(NSString*)strLbl{
    CGSize maximumSize = CGSizeMake(300, 9999);
    NSString *myString = strLbl;
    UIFont *myFont = [UIFont fontWithName:@"Helvetica" size:14];
    CGSize myStringSize = [myString sizeWithFont:myFont
                               constrainedToSize:maximumSize
                                   lineBreakMode:self.myLbl.lineBreakMode];
    return myStringSize;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
