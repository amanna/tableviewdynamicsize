//
//  ViewController.m
//  TableviewSizeautomatic
//
//  Created by MAPPS MAC on 02/06/16.
//  Copyright © 2016 Delgence. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"
@interface ViewController (){
    CustomCell *cell;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *str1 = @"Aditi";
    NSString *str2 = @" is a good";
    NSString *str3 = @"girl";
    
    NSString *final = [NSString stringWithFormat:@"%@%@%@",str1,str2,str3];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:final];
    [text addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0, str1.length)];
    [text addAttribute: NSForegroundColorAttributeName value: [UIColor greenColor] range: NSMakeRange(str1.length, str2.length)];
    [self.lblName setAttributedText: text];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        [tableView registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    }
    if(indexPath.row==0){
    cell.myLbl.numberOfLines = 0;
    cell.myLbl.font= [UIFont fontWithName:@"Helvetica" size:14];
    cell.myLbl.text = @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";
    [cell.myLbl sizeToFit];
    }else{
        cell.myLbl.text=@"dd";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0){
        NSString *str = @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";
        CGSize maximumSize = [cell sizeOfFloatCell:str];
        return maximumSize.height+ 5;
    }else{
        return 40;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;    //count number of row from counting array hear cataGorry is An Array
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
